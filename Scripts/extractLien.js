var extractDomaine = require("./extractDomaine");

var lienParser = function(urlServer, port){
	this.urlServer=urlServer==null ? "http://127.0.0.1" : urlServer;
	this.port = port==null ? 5000 : port
	this.domaine="";
	this.isSameDomaine=false;

	this.retrouveUrlComplet = function(url){
		if(url.indexOf("javascript:void")!=-1)
			return false;
		else if(url.substring(0,4)=="http"){
			domaine = extractDomaine(url);
			if(domaine==this.domaine){
				return url;
			}else{
				return false;
			}
		}else if(url.substring(0,2)=="//"){
			return false;
		}else if(url.substring(0,1)=="/"){
			return "http://"+this.domaine+url;
		}else{
			return "http://"+this.domaine+"/"+url;
		}
	}


	this.encodeUrl=function(url){
		this.isSameDomaine = this.retrouveUrlComplet(url) ? true : false;
		if(this.isSameDomaine)
			return this.urlServer+":"+this.port+"/goto?url="+encodeURI(this.retrouveUrlComplet(url));
		else
			return url;
	}
}

module.exports = lienParser;