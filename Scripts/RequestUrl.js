var cheerio = require('cheerio');
var request = require('request');
var lien = require("./extractLien");
var extractDomaine = require("./extractDomaine");


const EventEmitter = require('events');
const util = require('util');



var RequestUrl = function(url){
	EventEmitter.call(this);
	this.url = url;
	this.niveau=0;
	this.PILE_AFAIRE = new Array();
	this.PILE_FAIT = new Array();
	this.setUrl = function(url){
		this.url = url
	}

	this.start = function(){
		var object = this;
		request(this.url, function(error, response, html) {
			if (!error && response.statusCode == 200) {
				object.emit('terminer', html, object);
			}
		});
		
	}
}

util.inherits(RequestUrl, EventEmitter);




function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}


function getUrl(html, object){
	$ = cheerio.load(html);
	var lienParser = new lien();
	lienParser.domaine = extractDomaine(object.url);

	$("a").each(function(key, value){
		var url = $(this).attr("href");
		//console.log(url);
		try{
			var sameDomaine = lienParser.retrouveUrlComplet(url) ? true : false;
			if(sameDomaine)
				object.PILE_AFAIRE.push(lienParser.retrouveUrlComplet(url))
		}catch(e){
			//console.log(e);
		}
	});
	// il faut voir les doublons
	var length = object.PILE_AFAIRE.length;
	var url = "";
	for(var i=0;i<length;i++){
		url = object.PILE_AFAIRE.pop();
		if(!inArray(url, object.PILE_FAIT))
			object.PILE_FAIT.push(url);
	}

	
}


module.exports = RequestUrl;
module.exports.getUrl = getUrl
