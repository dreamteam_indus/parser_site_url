// NB : C'est auto instancier !
var extractPage = new function(){
	this.url = "";
	this.domaine="";

	this.setDomaine=function(url){
		this.domaine = extractDomain(url);
		return this;
	}

	this.getAllUrl=function(url){
		LienParser.domaine=this.domaine;
		var liens = new Array();

		// Requet est Asynchrone et fonctionne donc comme un threat !
		var req = new RequestUrl(url);
		req.start();
		setTimeout(function(){
			liens = req.listeUrl;
		}, 3000);
		return new liens;
	}

	this.getRecUrls = function(url, niveau){
		// fonction recursive limiter
		var maxNiveau = 2;
		if(niveau>maxNiveau)
			return new Array();
		var liens = new Array();
		var object = this;
		console.log(niveau);
		this.getAllUrl(url).each(function(key, value){
			// ici on applique la recursivite
			// et on étend le tableau aux autres url trouvés
			liens.extend(object.getRecUrls(value, niveau+1));
		});

		return this.clearDoublons(liens);
	}

	this.clearDoublon = function(listeLiens){
		// efface tout les doublons des liens
		var liens = listeLiens.filter(function(item, pos) {
		    return listeLiens.indexOf(item) == pos;
		});
		return liens;
	}

	
}


module.exports = extractPage;