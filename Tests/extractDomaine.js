function extractDomaine(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];

    return domain;
}




console.log(extractDomaine("http://w.dom.com"));
console.log(extractDomaine("https://w.dom.com/gz/zef"));
console.log(extractDomaine("ftp://w.dom.com"));
console.log(extractDomaine("jdbc:mysql://w.dom.com"));
console.log(extractDomaine("dom.com"));
console.log(extractDomaine("ww.dd.gg.ee/qd/sqd;dom.com"));
console.log(extractDomaine("zd;rzg"));
