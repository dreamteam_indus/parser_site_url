var extractDomaine = require("../Scripts/extractDomaine");

var Lien = new function(urlServer, port){
	this.urlServer=urlServer==null ? "http://127.0.0.1" : urlServer;
	this.port = port==null ? 5000 : port
	this.domaine="";
	this.isSameDomaine=false;

	this.retrouveUrlComplet = function(url){
		if(url.substr(0,4)=="http"){
			domaine = extractDomaine(url);
			if(domaine==this.domaine){
				return url;
			}else{
				return false;
			}
		}else if(url.substr(0,1)=="/"){
			return "http://"+this.domaine+url;
		}else{
			return "http://"+this.domaine+"/"+url;
		}
	}


	this.encodeUrl=function(url){
		this.isSameDomaine = this.retrouveUrlComplet(url) ? true : false;
		if(this.isSameDomaine)
			return this.urlServer+":"+this.port+"/goto?url="+encodeURI(this.retrouveUrlComplet(url));
		else
			return url;
	}
}


console.log(Lien.retrouveUrlComplet("http://w.dom.com"));
console.log(Lien.retrouveUrlComplet("https://w.dom.com/gz/zef"));
console.log(Lien.retrouveUrlComplet("ftp://w.dom.com"));
console.log(Lien.retrouveUrlComplet("jdbc:mysql://w.dom.com"));
console.log(Lien.retrouveUrlComplet("dom.com"));
console.log(Lien.retrouveUrlComplet("ww.dd.gg.ee/qd/sqd;dom.com"));
console.log(Lien.retrouveUrlComplet("zd;rzg"));