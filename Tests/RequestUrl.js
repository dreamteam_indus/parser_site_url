var cheerio = require('cheerio');
var request = require('request');
var lien = require("../Scripts/extractLien");
var extractDomaine = require("../Scripts/extractdomaine");


const EventEmitter = require('events');
const util = require('util');


const PILE_AFAIRE = new Array();
const PILE_FAIT = new Array();
const TERMINER=false;


var RequestUrl = function(url){
	EventEmitter.call(this);
	this.url = url;
	this.niveau = 0;

	this.start = function(){
		var object = this;
		request(this.url, function(error, response, html) {
			if (!error && response.statusCode == 200) {
				object.emit('terminer', html, object);
			}
		});
		
	}
}

util.inherits(RequestUrl, EventEmitter);




function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}


function getUrl(html, object){
	$ = cheerio.load(html);
	var lienParser = new lien();
	lienParser.domaine = extractDomaine(object.url);

	$("a").each(function(key, value){
		var url = $(this).attr("href");
		//console.log(url);
		try{
			var sameDomaine = lienParser.retrouveUrlComplet(url) ? true : false;
			if(sameDomaine)
				PILE_AFAIRE.push(lienParser.retrouveUrlComplet(url))
		}catch(e){
			//console.log(e);
		}
	});
	// il faut voir les doublons
	var length = PILE_AFAIRE.length;
	var url = "";
	for(var i=0;i<length;i++){
		url = PILE_AFAIRE.pop();
		if(!inArray(url, PILE_FAIT))
			PILE_FAIT.push(url);
		
			
	}
	TERMINER=true;
	
}




const requestUrl = new RequestUrl();
requestUrl.on('terminer', getUrl);






for(var i=0;i<100;i++){
	requestUrl.url = "http://www.tutorialspoint.com/nodejs/nodejs_event_loop.htm";
	requestUrl.start();
}

setTimeout(function(){
	console.log("FAIT : "+PILE_FAIT.length);
	console.log("AFAIRE : "+PILE_AFAIRE.length);
}, 1000);





